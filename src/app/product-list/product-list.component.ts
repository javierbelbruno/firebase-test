import { Component, OnInit } from '@angular/core';

import Product from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';
import { map } from 'rxjs/operators';

import { CartService } from '../cart.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent  {
  
  products?: Product[];
 
  
  constructor(
    private cartService: CartService,
    private productService: ProductService
    ) { }

  ngOnInit(): void {
    this.retrieveProducts();
    console.log(this.cartService.retrieveShippingPrices());
    console.log(this.cartService.shipping_prices)
    
  }

  retrieveProducts(): void {
    var shopId = ((this.cartService.getSelectedShop()!= null) ? this.cartService.getSelectedShop().id : 0);;

    this.productService.getAll().snapshotChanges().pipe(
      map((changes) =>
        changes.map(c =>
          ({ key: c.payload.key, ...c.payload.val() })
        ).filter(c => c.shop == shopId)
      )
      ).subscribe((data) => {
      this.products = data;
    });
  }

  addToCart(product: Product) {
    if(this.cartService.getCurrentVolume() + product.volume >= 100) {
      window.alert("Delivery Backpack is on %"+this.cartService.getCurrentVolume()+" of its capacity! This item can't be added! Nevertheless, you can pick smaller products.");
    }
    else {
      this.cartService.addToCart(product);
      window.alert('Your product has been added to the cart!');
    }
  }

}
