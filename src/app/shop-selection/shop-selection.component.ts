import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import Shop from 'src/app/models/shop.model';
import { ShopService } from 'src/app/services/shop.service';
import { map } from 'rxjs/operators';

import { CartService } from '../cart.service';



@Component({
  selector: 'app-shop-selection',
  templateUrl: './shop-selection.component.html',
  styleUrls: ['./shop-selection.component.css']
})
export class ShopSelectionComponent implements OnInit {

  shops?: Shop[];
  
  
  constructor(private route: ActivatedRoute,
            private cartService: CartService,
            private shopService: ShopService,
            public router: Router) { }

  ngOnInit(): void {
    this.retrieveProducts();
    console.log(this.cartService.getSelectedShop());
  }


  shopSelected(shopSelected) {
    if(this.cartService.getSelectedShop() == null || this.cartService.getSelectedShop() == undefined){
    this.cartService.setSelectedShop(shopSelected);
    this.router.navigate( ['/products']);
    }
    else{
      if(shopSelected.id == this.cartService.getSelectedShop().id){
        this.router.navigate( ['/products']);
      }else{
      window.alert('Your selected shop is ' + this.cartService.getSelectedShop().name + ", You are unable to add other shop items to cart");
      }
    }
    
  }

  retrieveProducts(): void {
    this.shopService.getAll().snapshotChanges().pipe(
      map((changes) =>
        changes.map(c =>
          ({ key: c.payload.key, ...c.payload.val() })
        )
      )
      ).subscribe((data) => {
      this.shops = data;
    });
  }

}
